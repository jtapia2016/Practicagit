﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(plicacion_ejemplo.Startup))]
namespace plicacion_ejemplo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
